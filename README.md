# Vulkan Fluid Dynamics Simulator

## Running

- Clone the repository.
- Install the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/) if not already installed.
- Install [vcpkg](https://vcpkg.io/en/getting-started.html) if not already installed.
- Generate the build files (replace `[vcpkg-root]` with the root path to your vcpkg installation):

`cmake -DCMAKE_TOOLCHAIN_FILE=[vcpkg-root]/scripts/buildsystems/vcpkg.cmake -B build -S .`

- Build the project:

`cmake --build build`

- Compile the shaders (requires glslc in your PATH):

`./compile.sh`

- Run the application:

`./vkFDSim`
