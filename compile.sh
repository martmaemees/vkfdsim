#!/bin/sh

BASE_DIR=$(dirname "$0")
OUT_DIR=$BASE_DIR/spirv
SRC_DIR=$BASE_DIR/shaders

mkdir -p $OUT_DIR
glslc $SRC_DIR/shader.vert -o $OUT_DIR/shader.vert.spv
glslc $SRC_DIR/shader.frag -o $OUT_DIR/shader.frag.spv
