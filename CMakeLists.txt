cmake_minimum_required(VERSION 3.16)
set(PROJECT_NAME vkFDSim)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
project(${PROJECT_NAME})

set(CMAKE_CXX_STANDARD 17)

# set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
# set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
# set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
# add_subdirectory(lib/glfw)

find_package(glfw3 REQUIRED)
find_package(glm REQUIRED)
find_package(Vulkan REQUIRED)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})

add_executable(${PROJECT_NAME}
        src/main.cpp
        src/Application.hpp src/Application.cpp src/Window.hpp src/Window.cpp
        src/DebugLogger.hpp src/DebugLogger.cpp src/Vertex.hpp
        src/Renderer.cpp src/Renderer.hpp src/VkUtils.cpp src/VkUtils.hpp
        src/Mesh.cpp src/Mesh.hpp src/Texture.cpp src/Texture.hpp
        src/FluidDynamics/FluidDynamics.h src/FluidDynamics/FluidDynamics.cpp
        src/FluidDynamics/FluidMatrix.h src/FluidDynamics/FluidMatrix.cpp
)
target_link_libraries(${PROJECT_NAME} glfw glm::glm Vulkan::Vulkan)
#target_precompile_headers(${PROJECT_NAME}
#    PRIVATE
#        <string>
#        <iostream>
#        <vector>
#        <array>
#        <chrono>
#        <optional>
#        <stdexcept>
#        <algorithm>
#
#        <GLFW/glfw3.h>
#
#        "pch/vulkan.hpp"
#        "pch/glm.hpp"
#)

target_include_directories(${PROJECT_NAME} PRIVATE src pch)
