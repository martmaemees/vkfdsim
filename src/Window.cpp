#include "Window.hpp"

Window::Window(uint32_t width, uint32_t height, std::string title)
{
    if (!glfwInit()) {
        throw std::runtime_error("Failed to initialize GLFW.");
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (window == nullptr) {
        glfwTerminate();
        throw std::runtime_error("Failed to create a window!");
    }

    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}

Window::~Window()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void Window::framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
    auto classPtr = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    classPtr->setFramebufferResized(true);
}

vk::SurfaceKHR Window::createSurface(vk::Instance instance)
{
    VkSurfaceKHR _surface;
    if (glfwCreateWindowSurface(VkInstance(instance), window, nullptr, &_surface) != VK_SUCCESS) {
        throw std::runtime_error("GLFW failed to create a Vulkan surface.");
    }
    return vk::SurfaceKHR(_surface);
}
