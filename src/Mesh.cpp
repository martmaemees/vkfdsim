#include "Mesh.hpp"
#include "Application.hpp"
#include "VkUtils.hpp"

Mesh::~Mesh()
{
    application.device.destroyBuffer(vertexBuffer);
    application.device.freeMemory(vertexBufferMemory);
    application.device.destroyBuffer(indexBuffer);
    application.device.freeMemory(indexBufferMemory);
    std::cout << "Mesh resources freed" << std::endl;
}

void Mesh::createVertexBuffer()
{
    vk::DeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

    // Create and fill the staging buffer.
    vk::Buffer stagingBuffer;
    vk::DeviceMemory stagingBufferMemory;
    createBuffer(
        application.physicalDevice, application.device,
        bufferSize, vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
        stagingBuffer, stagingBufferMemory
    );
    void* data = application.device.mapMemory(stagingBufferMemory, 0, bufferSize);
    memcpy(data, vertices.data(), (size_t) bufferSize);
    application.device.unmapMemory(stagingBufferMemory);

    // Create final vertex buffer.
    createBuffer(
        application.physicalDevice, application.device,
        bufferSize, vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eDeviceLocal,
        vertexBuffer, vertexBufferMemory
    );

    // Copy from staging to vertex buffer.
    vk::CommandBuffer cb = application.device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
        application.commandPool, vk::CommandBufferLevel::ePrimary, 1
    ))[0];
    cb.begin(vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit));
    cb.copyBuffer(stagingBuffer, vertexBuffer, vk::BufferCopy(0, 0, bufferSize));
    cb.end();
    application.graphicsQueue.submit(vk::SubmitInfo(
        0, nullptr, {}, 1, &cb
    ), nullptr);
    application.graphicsQueue.waitIdle();
    application.device.freeCommandBuffers(application.commandPool, cb);

    // Cleanup staging buffer.
    application.device.destroyBuffer(stagingBuffer);
    application.device.freeMemory(stagingBufferMemory);
}

void Mesh::createIndexBuffer()
{
    vk::DeviceSize bufferSize = sizeof(indices[0]) * indices.size();

    // Create and fill the staging buffer.
    vk::Buffer stagingBuffer;
    vk::DeviceMemory stagingBufferMemory;
    createBuffer(
        application.physicalDevice, application.device,
        bufferSize, vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
        stagingBuffer, stagingBufferMemory
    );
    void* data = application.device.mapMemory(stagingBufferMemory, 0, bufferSize);
    memcpy(data, indices.data(), (size_t) bufferSize);
    application.device.unmapMemory(stagingBufferMemory);

    // Create final index buffer.
    createBuffer(
        application.physicalDevice, application.device,
        bufferSize, vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eDeviceLocal,
        indexBuffer, indexBufferMemory
    );

    // Copy from staging to index buffer.
    vk::CommandBuffer cb = application.device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
        application.commandPool, vk::CommandBufferLevel::ePrimary, 1
    ))[0];
    cb.begin(vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit));
    cb.copyBuffer(stagingBuffer, indexBuffer, vk::BufferCopy(0, 0, bufferSize));
    cb.end();
    application.graphicsQueue.submit(vk::SubmitInfo(
        0, nullptr, {}, 1, &cb
    ), nullptr);
    application.graphicsQueue.waitIdle();
    application.device.freeCommandBuffers(application.commandPool, cb);

    // Cleanup staging buffer.
    application.device.destroyBuffer(stagingBuffer);
    application.device.freeMemory(stagingBufferMemory);
}
