#pragma once
#include "pch.hpp"

class Application;

class Texture
{
public:
    Texture(const Application& application) : application{application} { }
    ~Texture() {
        cleanup();
    }

    vk::DescriptorSet* getDescriptorSet(uint32_t frame) {
        return &descriptorSets[frame];
    }

    void initTexture(uint32_t width, uint32_t height, uint32_t framesInFlight, vk::DescriptorSetLayout descriptorSetLayout);
    void bufferTexture(const float* data, uint32_t frame, vk::Fence& waitFor);
    void cleanup();

private:
    const Application& application;
    uint32_t framesInFlight;
    uint32_t width, height;

    std::vector<vk::Image> images;
    std::vector<vk::DeviceMemory> imageMemories;
    std::vector<vk::ImageView> imageViews;
    vk::Sampler sampler;

    vk::DeviceSize bufferSize;
    std::vector<vk::Buffer> stagingBuffers;
    std::vector<vk::DeviceMemory> stagingBufferMemories;
    std::vector<vk::CommandBuffer> transferCommandBuffers;

    vk::DescriptorSetLayout descriptorSetLayout;
    vk::DescriptorPool descriptorPool;
    std::vector<vk::DescriptorSet> descriptorSets;

    void initDescriptors();
};
