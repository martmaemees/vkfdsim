#pragma once

#include "pch.hpp"

class Window {
  public:
    Window(uint32_t width, uint32_t height, std::string title);
    ~Window();

    GLFWwindow* getGLFWWindow() const { return window; }

    bool isFramebufferResized() const { return framebufferResized; }
    void setFramebufferResized(bool value) { framebufferResized = value; }

    vk::SurfaceKHR createSurface(vk::Instance instance);

  private:
    GLFWwindow* window;
    uint32_t width, height;
    bool framebufferResized = false;

    static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
};
