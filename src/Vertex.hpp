#pragma once

#include "pch.hpp"

struct Vertex {
    glm::vec2 pos;
    glm::vec2 texCoord;

    static vk::VertexInputBindingDescription getBindingDescription()
    {
        return vk::VertexInputBindingDescription(0, sizeof(Vertex));
    }

    static std::array<vk::VertexInputAttributeDescription, 2> getAttributeDescriptions()
    {
        std::array<vk::VertexInputAttributeDescription, 2> attrDescs;

        attrDescs[0].binding = 0;
        attrDescs[0].location = 0;
        attrDescs[0].format = vk::Format::eR32G32Sfloat;
        attrDescs[0].offset = offsetof(Vertex, pos);

        attrDescs[1].binding = 0;
        attrDescs[1].location = 1;
        attrDescs[1].format = vk::Format::eR32G32Sfloat;
        attrDescs[1].offset = offsetof(Vertex, texCoord);

        return attrDescs;
    }
};
