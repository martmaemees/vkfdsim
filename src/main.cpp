#include "Application.hpp"
#include "Mesh.hpp"
#include "Texture.hpp"

#include <FluidDynamics/FluidMatrix.h>
#include <cstdlib>
#include <ctime>
#include <vector>

constexpr uint32_t MATRIX_SIZE = 254;

class FDSim : public Application {
  public:
    FDSim(uint32_t width, uint32_t height, const std::string& title) : Application(width, height, title) {}

    virtual ~FDSim() {}

  protected:
    void start() override {
        std::vector<Vertex> vertices = {Vertex{glm::vec2{-1.0f, -1.0f}, glm::vec2{0.0f, 1.0f}},
                                        Vertex{glm::vec2{-1.0f, 1.0f}, glm::vec2{0.0f, 0.0f}},
                                        Vertex{glm::vec2{1.0f, 1.0f}, glm::vec2{1.0f, 0.0f}},
                                        Vertex{glm::vec2{1.0f, -1.0f}, glm::vec2{1.0f, 1.0f}}};
        std::vector<uint32_t> indices = {0, 1, 2, 0, 2, 3};
        planeMesh = new Mesh((Application&)*this, vertices, indices);
        planeMesh->init();

        densityDataTexture = renderer.createTexture(MATRIX_SIZE + 2, MATRIX_SIZE + 2);

        matrix = new FluidMatrix(MATRIX_SIZE);
        matrix->SetDensitySource(MATRIX_SIZE / 2, MATRIX_SIZE / 2, 40.0f);
        matrix->SetVelocitySource(MATRIX_SIZE / 2, MATRIX_SIZE / 2, 10.0f, -5.0f);
    }

    void update(float deltaTime) override {
        matrix->TimeStep(deltaTime);
        renderer.bufferTextureData(*densityDataTexture, matrix->GetDens());

        std::vector<Geometry> geometry;
        geometry.push_back({planeMesh, densityDataTexture});
        renderer.drawGeometry(geometry);
    }

    void cleanup() override {
        delete planeMesh;
        delete densityDataTexture;
        delete matrix;

        Application::cleanup();
    }

    Mesh* planeMesh = nullptr;
    Texture* densityDataTexture = nullptr;
    FluidMatrix* matrix = nullptr;
};

int main() {
    srand(static_cast<unsigned>(time(0)));

    FDSim app(800, 600, "vkFDSim");
    app.run();
}
