//
// Created by Märt on 22-Dec-17.
//

#pragma once

#include <string>

class FluidMatrix
{
public:
    FluidMatrix(int size);
    ~FluidMatrix();

    void TimeStep(float dt);
    void SetDensitySource(int x, int y, float value);
    void SetVelocitySource(int x, int y, float xValue, float yValue);
    void ClearDensitySource(int x, int y);
    void ClearVelocitySource(int x, int y);
    void ResetDensitySources();
    void ResetVelocitySources();
    std::string ToDensityString() const;

    const float *GetDens() const
    {
        return dens;
    }

    const float *GetVx() const
    {
        return vx;
    }

    const float *GetVy() const
    {
        return vy;
    }

    const float GetDensAt(int x, int y) const
    {
        return dens[x + y * N];
    }

    const float GetVxAt(int x, int y) const
    {
        return vx[x + y * N];
    }

    const float GetVyAt(int x, int y) const
    {
        return vy[x + y * N];
    }

private:
    int size;   // Width of the matrix
    int N;      // Width of the matrix, including the unseen borders.

    float *dens, *vx, *vy;
    float *densPrev, *vxPrev, *vyPrev;
    float *densSources, *vxSources, *vySources;

    constexpr static float diff = 0.0001f;
    constexpr static float viscosity = 0.0001f;

    void CalculateDensity(float dt);
    void CalculateVelocity(float dt);
    void UpdateDensitySources(float dt);
    void UpdateVelocitySources(float dt);
};
