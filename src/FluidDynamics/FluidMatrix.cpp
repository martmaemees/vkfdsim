//
// Created by Märt on 22-Dec-17.
//

#include <iostream>
#include <sstream>
#include <iomanip>
#include "FluidDynamics.h"
#include "FluidMatrix.h"


#define SWAP(x0, x) {float *tmp=x0; x0=x; x=tmp;}
#define IX(i,j) ((i)+(N)*(j))

FluidMatrix::FluidMatrix(int size)
{
    this->size = size;
    N = size+2;

    dens = new float[N*N]();
    vx = new float[N*N]();
    vy = new float[N*N]();
    densPrev = new float[N*N]();
    vxPrev = new float[N*N]();
    vyPrev = new float[N*N]();

    densSources = new float[N*N]();
    vxSources = new float[N*N]();
    vySources = new float[N*N]();
}

FluidMatrix::~FluidMatrix()
{
    delete[] dens, vx, vy;
    delete[] densPrev, vxPrev, vyPrev;
    delete[] densSources, vxSources, vySources;
}

void FluidMatrix::TimeStep(float dt)
{
    CalculateVelocity(dt);
    CalculateDensity(dt);
}

void FluidMatrix::SetDensitySource(int x, int y, float value)
{
    if(x < 0 || x >= N || y < 0 || y >= N)
    {
        std::cerr << "Tried to set a density source outside the boundary of the FluidMatrix" << std::endl;
        return;
    }
    if(value < 0.0)
    {
        std::cerr << "Tried to set a negative density source." << std::endl;
        return;
    }
    densSources[IX(x, y)] = value;
}

void FluidMatrix::SetVelocitySource(int x, int y, float xValue, float yValue)
{
    if(x < 0 || x >= N || y < 0 || y >= N)
    {
        std::cerr << "Tried to set a velocity source outside the boundary of the FluidMatrix" << std::endl;
        return;
    }
    vxSources[IX(x, y)] = xValue;
    vySources[IX(x, y)] = yValue;
}

void FluidMatrix::ClearDensitySource(int x, int y)
{
    if(x < 0 || x >= N || y < 0 || y >= N)
    {
        std::cerr << "Tried to clear a velocity source outside the boundary of the FluidMatrix" << std::endl;
        return;
    }
    densSources[IX(x, y)] = 0;
}

void FluidMatrix::ClearVelocitySource(int x, int y)
{
    if(x < 0 || x >= N || y < 0 || y >= N)
    {
        std::cerr << "Tried to clear a velocity source outside the boundary of the FluidMatrix" << std::endl;
        return;
    }
    vxSources[IX(x, y)] = 0;
    vySources[IX(x, y)] = 0;
}

void FluidMatrix::CalculateDensity(float dt)
{
    UpdateDensitySources(dt);
    SWAP(dens, densPrev);
    Diffuse(size, 0, dens, densPrev, diff, dt);
    SWAP(dens, densPrev);
    Advect(size, 0, dens, densPrev, vx, vy, dt);
}

void FluidMatrix::CalculateVelocity(float dt)
{
    UpdateVelocitySources(dt);

    SWAP(vx, vxPrev);
    Diffuse(size, 1, vx, vxPrev, viscosity, dt);
    SWAP(vy, vyPrev);
    Diffuse(size, 2, vy, vyPrev, viscosity, dt);

    Project(size, vx, vy, vxPrev, vyPrev);

    SWAP(vx, vxPrev);
    SWAP(vy, vyPrev);
    Advect(size, 1, vx, vxPrev, vxPrev, vyPrev, dt);
    Advect(size, 2, vy, vyPrev, vxPrev, vyPrev, dt);

    Project(size, vx, vy, vxPrev, vyPrev);
}

void FluidMatrix::UpdateDensitySources(float dt)
{
    for(int x = 0; x < N; x++) {
        for(int y = 0; y < N; y++) {
            dens[IX(x, y)] += densSources[IX(x, y)] * dt;
        }
    }
}

void FluidMatrix::UpdateVelocitySources(float dt)
{
    for(int x = 0; x < N; x++) {
        for(int y = 0; y < N; y++) {
            vx[IX(x, y)] += vxSources[IX(x, y)] * dt;
            vy[IX(x, y)] += vySources[IX(x, y)] * dt;
        }
    }
}

std::string FluidMatrix::ToDensityString() const
{
    std::stringstream result;
    result << std::fixed << std::setprecision(1);
    for(int y = 0; y < N; y++) {
        for(int x = 0; x < N; x++) {
            result << dens[IX(x, y)] << " ";
        }
        result << std::endl;
    }
    return result.str();
}

void FluidMatrix::ResetDensitySources()
{
    std::fill(densSources, densSources+N*N, 0.0);
}

void FluidMatrix::ResetVelocitySources()
{
    std::fill(vxSources, vxSources+N*N, 0.0);
    std::fill(vySources, vySources+N*N, 0.0);
}
