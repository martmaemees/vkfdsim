//
// Created by Märt on 22-Dec-17.
// Algorithm based on Jos Stam's paper "Real-Time Fluid Dynamics for Games"
// http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
//

#pragma once

void Diffuse(int N, int b, float *x, float *x0, float diff, float dt);

void Advect(int N, int b, float *d, float *d0, float *u, float *v, float dt);

void Project(int N, float *u, float *v, float *p, float *div);

void SetBnd(int N, int b, float *x);