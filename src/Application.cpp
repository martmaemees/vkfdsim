#include "Application.hpp"
#include "DebugLogger.hpp"

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

const std::vector<const char*> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

void Application::run()
{
    start();
    lastFrameTime = static_cast<float>(glfwGetTime());
    while (!glfwWindowShouldClose(window.getGLFWWindow())) {
        glfwPollEvents();
        float currentTime = static_cast<float>(glfwGetTime());
        float deltaTime = currentTime - lastFrameTime;
        lastFrameTime = currentTime;
        update(deltaTime);
    }
    device.waitIdle();
    cleanup();
}

void Application::initVulkan()
{
    // Instance independent function pointers.
    vk::DynamicLoader dl;
    VULKAN_HPP_DEFAULT_DISPATCHER.init(dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr"));

    createInstance();
    surface = window.createSurface(instance);
    physicalDevice = getBestPhysicalDevice();
    createLogicalDevice();
    createCommandPool();

    renderer.initRenderer();
}

void Application::cleanup()
{
    renderer.cleanup();
    device.destroyCommandPool(commandPool);
    device.destroy();
    instance.destroySurfaceKHR(surface);
    instance.destroyDebugUtilsMessengerEXT(debugMessenger);
    instance.destroy();
}

void Application::createInstance()
{
    auto extensions = getRequiredExtensions();
    vk::ApplicationInfo appInfo(
        title.c_str(), VK_MAKE_VERSION(1, 0, 0), "No Engine", VK_MAKE_VERSION(1, 0, 0), VK_API_VERSION_1_0
    );
    vk::InstanceCreateInfo createInfo(
        {}, &appInfo, 0, {}, static_cast<uint32_t>(extensions.size()), extensions.data()
    );

    #ifndef NDEBUG
    if (!checkValidationLayerSupport()) {
        throw std::runtime_error("Validation layers requested, but not available!");
    }
    createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    createInfo.ppEnabledLayerNames = validationLayers.data();
    auto debugMessengerInfo = debugMessengerCreateInfo();
    createInfo.pNext = &debugMessengerInfo;
    std::cout << "Validation layers enabled!" << std::endl;
    #endif

    instance = vk::createInstance(createInfo);

    VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);

    #ifndef NDEBUG
    debugMessenger = instance.createDebugUtilsMessengerEXT(debugMessengerInfo);
    #endif
}

vk::PhysicalDevice Application::getBestPhysicalDevice()
{
    std::vector<vk::PhysicalDevice> devices = instance.enumeratePhysicalDevices();
    std::multimap<int, vk::PhysicalDevice> candidates;

    for (const auto& device : devices) {
        int score = ratePhysicalDevice(device);
        candidates.insert(std::make_pair(score, device));
    }

    if (candidates.rbegin()->first > 0) {
        return candidates.rbegin()->second;
    }
    throw std::runtime_error("Failed to find a suitable GPU!");
}

void Application::createLogicalDevice()
{
    auto indices = QueueFamilyIndices::findQueueFamilies(physicalDevice, surface);

    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = {indices.graphicsFamily.value(), indices.presentFamily.value()};

    float queuePriority = 1.0f;
    for(uint32_t queueFamily : uniqueQueueFamilies) {
        queueCreateInfos.push_back(vk::DeviceQueueCreateInfo(
            {}, queueFamily, 1, &queuePriority
        ));
    }
    vk::PhysicalDeviceFeatures deviceFeatures;

    vk::DeviceCreateInfo createInfo(
        {}, static_cast<uint32_t>(queueCreateInfos.size()), queueCreateInfos.data(),
        0, {},
        static_cast<uint32_t>(deviceExtensions.size()), deviceExtensions.data(), &deviceFeatures
    );

    device = physicalDevice.createDevice(createInfo);
    VULKAN_HPP_DEFAULT_DISPATCHER.init(device);
    graphicsQueue = device.getQueue(indices.graphicsFamily.value(), 0);
    presentQueue = device.getQueue(indices.presentFamily.value(), 0);
}

void Application::createCommandPool()
{
    auto indices = QueueFamilyIndices::findQueueFamilies(physicalDevice, surface);

    vk::CommandPoolCreateInfo poolInfo(vk::CommandPoolCreateFlagBits::eResetCommandBuffer, indices.graphicsFamily.value());
    commandPool = device.createCommandPool(poolInfo);
}

std::vector<const char*> Application::getRequiredExtensions()
{
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    #ifndef NDEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    #endif

    return extensions;
}

uint32_t Application::ratePhysicalDevice(vk::PhysicalDevice physDevice)
{
    int score = 0;
    auto properties = physDevice.getProperties();
    auto features = physDevice.getFeatures();

    switch (properties.deviceType) {
        case vk::PhysicalDeviceType::eDiscreteGpu:
            score += 1000;
            break;
        case vk::PhysicalDeviceType::eIntegratedGpu:
            score += 100;
            break;
    }
    score += properties.limits.maxImageDimension2D * 0.1;

    auto indices = QueueFamilyIndices::findQueueFamilies(physDevice, surface);
    if (!indices.isComplete()) { return 0; }
    if (!checkDeviceExtensionSupport(physDevice)) { return 0; }
    auto swapchainSupport = querySwapchainSupport(physDevice, surface);
    if (swapchainSupport.formats.empty() || swapchainSupport.presentModes.empty()) {
        return 0;
    }

    return score;
}

QueueFamilyIndices QueueFamilyIndices::findQueueFamilies(vk::PhysicalDevice physDevice, vk::SurfaceKHR surface)
{
    QueueFamilyIndices indices;

    std::vector<vk::QueueFamilyProperties> queueFamilies = physDevice.getQueueFamilyProperties();
    int i = 0;
    for (const auto& queueFamily : queueFamilies) {
        if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
            indices.graphicsFamily = i;
        }
        if (physDevice.getSurfaceSupportKHR(i, surface)) {
            indices.presentFamily = i;
        }

        if (indices.isComplete()) {
            break;
        }
        i++;
    }

    return indices;
}

bool Application::checkDeviceExtensionSupport(vk::PhysicalDevice device)
{
    std::vector<vk::ExtensionProperties> extensions = device.enumerateDeviceExtensionProperties();
    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
    for (const auto& extension : extensions) {
        requiredExtensions.erase(extension.extensionName.begin());
    }
    return requiredExtensions.empty();
}
