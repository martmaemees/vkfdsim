#pragma once

#include <vulkan/vulkan.hpp>

const std::vector<const char*> validationLayers = {
        "VK_LAYER_KHRONOS_validation",
//        "VK_LAYER_LUNARG_monitor" // FPS Monitor
};

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
        void* pUserData);

vk::DebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo();

bool checkValidationLayerSupport();
