#include "Renderer.hpp"
#include "Application.hpp"
#include "Vertex.hpp"

void Renderer::initRenderer() {
    createSwapchain();
    createRenderPass();
    createDescriptorSetLayout();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandBuffers();
    createSyncObjects();
}

void Renderer::cleanup() {
    for (uint32_t i = 0; i < maxFramesInFlight; i++) {
        application.device.destroySemaphore(imageAvailableSemaphores[i]);
        application.device.destroySemaphore(renderFinishedSemaphores[i]);
        application.device.destroyFence(inFlightFences[i]);
    }

    for (auto framebuffer : swapchainFramebuffers) {
        application.device.destroyFramebuffer(framebuffer);
    }
    application.device.freeCommandBuffers(application.commandPool, commandBuffers);

    application.device.destroyPipeline(graphicsPipeline);
    application.device.destroyPipelineLayout(pipelineLayout);
    application.device.destroyRenderPass(renderPass);
    for (auto& imgView : swapchainImageViews) {
        application.device.destroyImageView(imgView);
    }
    application.device.destroySwapchainKHR(swapchain);
}

void Renderer::drawGeometry(std::vector<Geometry> geom) {
    application.device.waitForFences(1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

    vk::CommandBuffer cb = commandBuffers[currentFrame];

    cb.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
    cb.begin(vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit));

    // FIXME: Multiple result values.
    vk::ResultValue<uint32_t> imageIndexRes =
        application.device.acquireNextImageKHR(swapchain, UINT64_MAX, imageAvailableSemaphores[currentFrame], nullptr);
    uint32_t imageIndex = imageIndexRes.value;

    // Record command buffer.
    vk::RenderPassBeginInfo renderPassInfo(renderPass, swapchainFramebuffers[imageIndex],
                                           vk::Rect2D(vk::Offset2D(0, 0), swapchainExtent), 1,
                                           (vk::ClearValue*)(&clearColor));
    cb.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
    cb.bindPipeline(vk::PipelineBindPoint::eGraphics, graphicsPipeline);

    // Start of render pass.
    for (auto& g : geom) {
        cb.bindVertexBuffers(0, g.mesh->getVertexBuffer(), {0});
        if (g.texture.has_value()) {
            cb.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipelineLayout, 0, 1,
                                  g.texture.value()->getDescriptorSet(currentFrame), 0, nullptr);
        }
        cb.bindIndexBuffer(g.mesh->getIndexBuffer(), 0, vk::IndexType::eUint32);
        cb.drawIndexed(g.mesh->getIndexCount(), 1, 0, 0, 0);
    }

    cb.endRenderPass();
    cb.end();

    application.device.resetFences(inFlightFences[currentFrame]);

    vk::Semaphore waitSemaphores[] = {imageAvailableSemaphores[currentFrame]};
    vk::Flags<vk::PipelineStageFlagBits> waitStages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    vk::Semaphore signalSemaphores[] = {renderFinishedSemaphores[currentFrame]};
    vk::SubmitInfo submitInfo(1, waitSemaphores, waitStages, 1, &cb, 1, signalSemaphores);
    application.graphicsQueue.submit(submitInfo, inFlightFences[currentFrame]);

    vk::SwapchainKHR swapchains[] = {swapchain};
    vk::PresentInfoKHR presentInfo(1, signalSemaphores, 1, swapchains, &imageIndex, nullptr);
    application.presentQueue.presentKHR(presentInfo);

    currentFrame = (currentFrame + 1) % maxFramesInFlight;
}

Texture* Renderer::createTexture(uint32_t width, uint32_t height) {
    Texture* tex = new Texture(application);
    tex->initTexture(width, height, maxFramesInFlight, descriptorSetLayout);
    return tex;
}

void Renderer::bufferTextureData(Texture& tex, const float* data) {
    tex.bufferTexture(data, currentFrame, inFlightFences[currentFrame]);
}

void Renderer::createSwapchain() {
    auto support = querySwapchainSupport(application.physicalDevice, application.surface);

    bool found = false;
    vk::SurfaceFormatKHR surfaceFormat;
    for (const auto& availableFormat : support.formats) {
        if (availableFormat.format == vk::Format::eR8G8B8A8Srgb &&
            availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
            surfaceFormat = availableFormat;
            found = true;
            break;
        }
    }
    if (!found) {
        surfaceFormat = support.formats[0];
    }

    found = false;
    vk::PresentModeKHR presentMode;
    for (const auto& availableMode : support.presentModes) {
        if (availableMode == vk::PresentModeKHR::eMailbox) {
            presentMode = availableMode;
            found = true;
            break;
        }
    }
    if (!found) {
        presentMode = vk::PresentModeKHR::eFifo;
    }

    vk::Extent2D swapExtent;
    if (support.capabilities.currentExtent.width != UINT32_MAX) {
        swapExtent = support.capabilities.currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(application.window.getGLFWWindow(), &width, &height);
        swapExtent = vk::Extent2D{static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
        swapExtent.width = std::max(support.capabilities.minImageExtent.width,
                                    std::min(support.capabilities.maxImageExtent.width, swapExtent.width));
        swapExtent.height = std::max(support.capabilities.minImageExtent.height,
                                     std::min(support.capabilities.maxImageExtent.height, swapExtent.height));
    }

    uint32_t imageCount = std::max(support.capabilities.minImageCount, maxFramesInFlight);
    if (support.capabilities.maxImageCount > 0 && imageCount > support.capabilities.maxImageCount) {
        imageCount = support.capabilities.maxImageCount;
    }
    if (imageCount < maxFramesInFlight) {
        maxFramesInFlight = imageCount; // Can't have more frames in flight than we have swapchain images.
    }

    vk::SwapchainCreateInfoKHR createInfo({}, application.surface, imageCount, surfaceFormat.format,
                                          surfaceFormat.colorSpace, swapExtent, 1,
                                          vk::ImageUsageFlagBits::eColorAttachment);
    auto indices = QueueFamilyIndices::findQueueFamilies(application.physicalDevice, application.surface);
    uint32_t queueFamilyIndices[] = {indices.graphicsFamily.value(), indices.presentFamily.value()};

    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = vk::SharingMode::eExclusive;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = nullptr;
    }
    createInfo.preTransform = support.capabilities.currentTransform;
    createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;

    swapchain = application.device.createSwapchainKHR(createInfo);
    swapchainImages = application.device.getSwapchainImagesKHR(swapchain);
    swapchainImageFormat = surfaceFormat.format;
    swapchainExtent = swapExtent;

    swapchainImageViews.resize(swapchainImages.size());
    for (size_t i = 0; i < swapchainImageViews.size(); i++) {
        vk::ImageViewCreateInfo imgViewInfo({}, swapchainImages[i], vk::ImageViewType::e2D, swapchainImageFormat, {},
                                            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
        swapchainImageViews[i] = application.device.createImageView(imgViewInfo);
    }
}

void Renderer::createRenderPass() {
    vk::AttachmentDescription colorAttachment({}, swapchainImageFormat, vk::SampleCountFlagBits::e1,
                                              vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
                                              vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
                                              vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR);
    vk::AttachmentReference colorAttachmentRef(0, vk::ImageLayout::eColorAttachmentOptimal);

    vk::SubpassDescription subpass({}, vk::PipelineBindPoint::eGraphics, 0, {}, 1, &colorAttachmentRef);
    vk::SubpassDependency dependency(VK_SUBPASS_EXTERNAL, 0, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::AccessFlagBits::eColorAttachmentWrite);

    vk::RenderPassCreateInfo renderPassInfo({}, 1, &colorAttachment, 1, &subpass, 1, &dependency);
    renderPass = application.device.createRenderPass(renderPassInfo);
}

void Renderer::createDescriptorSetLayout() {
    vk::DescriptorSetLayoutBinding samplerLayoutBinding(0, vk::DescriptorType::eCombinedImageSampler, 1,
                                                        vk::ShaderStageFlagBits::eFragment);
    vk::DescriptorSetLayoutCreateInfo layoutInfo({}, 1, &samplerLayoutBinding);
    descriptorSetLayout = application.device.createDescriptorSetLayout(layoutInfo);
}

void Renderer::createGraphicsPipeline() {
    auto vertShaderCode = readFileBinary("spirv/shader.vert.spv");
    auto fragShaderCode = readFileBinary("spirv/shader.frag.spv");

    vk::ShaderModuleCreateInfo vertModuleInfo({}, vertShaderCode.size(),
                                              reinterpret_cast<const uint32_t*>(vertShaderCode.data()));
    auto vertShaderModule = application.device.createShaderModule(vertModuleInfo);
    vk::ShaderModuleCreateInfo fragModuleInfo({}, fragShaderCode.size(),
                                              reinterpret_cast<const uint32_t*>(fragShaderCode.data()));
    auto fragShaderModule = application.device.createShaderModule(fragModuleInfo);

    std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages = {
        vk::PipelineShaderStageCreateInfo({}, vk::ShaderStageFlagBits::eVertex, vertShaderModule, "main"),
        vk::PipelineShaderStageCreateInfo({}, vk::ShaderStageFlagBits::eFragment, fragShaderModule, "main")};

    auto bindingDesc = Vertex::getBindingDescription();
    auto attrDesc = Vertex::getAttributeDescriptions();
    vk::PipelineVertexInputStateCreateInfo vertexInputInfo({}, 1, &bindingDesc, static_cast<uint32_t>(attrDesc.size()),
                                                           attrDesc.data());
    vk::PipelineInputAssemblyStateCreateInfo inputAssembly({}, vk::PrimitiveTopology::eTriangleList, VK_FALSE);

    vk::Viewport viewport(0.0f, 0.0f, (float)swapchainExtent.width, (float)swapchainExtent.height, 0.0f, 1.0f);
    vk::Rect2D scissor({0, 0}, swapchainExtent);
    vk::PipelineViewportStateCreateInfo viewportState({}, 1, &viewport, 1, &scissor);

    vk::PipelineRasterizationStateCreateInfo rasterizer({}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill,
                                                        vk::CullModeFlagBits::eBack, vk::FrontFace::eCounterClockwise,
                                                        VK_FALSE, 0.0f, 0.0f, 0.0f, 1.0f);
    vk::PipelineMultisampleStateCreateInfo multisampling({}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f);

    vk::PipelineColorBlendAttachmentState colorBlendAttachment(
        VK_FALSE, vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd, vk::BlendFactor::eOne,
        vk::BlendFactor::eZero, vk::BlendOp::eAdd,
        vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB |
            vk::ColorComponentFlagBits::eA);
    vk::PipelineColorBlendStateCreateInfo colorBlending({}, VK_FALSE, vk::LogicOp::eCopy, 1, &colorBlendAttachment);

    vk::PipelineLayoutCreateInfo pipelineLayoutInfo({}, 1, &descriptorSetLayout);
    pipelineLayout = application.device.createPipelineLayout(pipelineLayoutInfo);

    vk::GraphicsPipelineCreateInfo pipelineInfo(
        {}, static_cast<uint32_t>(shaderStages.size()), shaderStages.data(), &vertexInputInfo, &inputAssembly, {},
        &viewportState, &rasterizer, &multisampling, {}, &colorBlending, nullptr, pipelineLayout, renderPass, 0);

    // FIXME: Multiple success result values.
    graphicsPipeline = application.device.createGraphicsPipeline(nullptr, pipelineInfo).value;

    application.device.destroyShaderModule(vertShaderModule);
    application.device.destroyShaderModule(fragShaderModule);
}

void Renderer::createFramebuffers() {
    swapchainFramebuffers.resize(swapchainImageViews.size());

    for (size_t i = 0; i < swapchainImageViews.size(); i++) {
        std::array<vk::ImageView, 1> attachments = {swapchainImageViews[i]};
        vk::FramebufferCreateInfo framebufferInfo({}, renderPass, static_cast<uint32_t>(attachments.size()),
                                                  attachments.data(), swapchainExtent.width, swapchainExtent.height, 1);
        swapchainFramebuffers[i] = application.device.createFramebuffer(framebufferInfo);
    }
}

void Renderer::createCommandBuffers() {
    vk::CommandBufferAllocateInfo allocInfo(application.commandPool, vk::CommandBufferLevel::ePrimary,
                                            maxFramesInFlight);
    commandBuffers = application.device.allocateCommandBuffers(allocInfo);
}

void Renderer::createSyncObjects() {
    imageAvailableSemaphores.resize(maxFramesInFlight);
    renderFinishedSemaphores.resize(maxFramesInFlight);
    inFlightFences.resize(maxFramesInFlight);
    imagesInFlight.resize(maxFramesInFlight);

    for (size_t i = 0; i < maxFramesInFlight; i++) {
        imageAvailableSemaphores[i] = application.device.createSemaphore({});
        renderFinishedSemaphores[i] = application.device.createSemaphore({});
        inFlightFences[i] = application.device.createFence(vk::FenceCreateInfo(vk::FenceCreateFlagBits::eSignaled));
    }
}
