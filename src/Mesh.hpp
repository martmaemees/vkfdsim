#pragma once

#include "Vertex.hpp"

class Application;

class Mesh
{
public:
    Mesh(const Application& app, const std::vector<Vertex> vertices, const std::vector<uint32_t> indices)
        : application{app}, vertices{vertices}, indices{indices} { }
    ~Mesh();

    void init() {
        createVertexBuffer();
        createIndexBuffer();
    }

    const vk::Buffer getVertexBuffer() const { return vertexBuffer; }
    const vk::Buffer getIndexBuffer() const { return indexBuffer; }
    const uint32_t getIndexCount() const { return static_cast<uint32_t>(indices.size()); }

private:
    const Application& application;
    const std::vector<Vertex> vertices;
    const std::vector<uint32_t> indices;

    vk::Buffer vertexBuffer;
    vk::DeviceMemory vertexBufferMemory;
    vk::Buffer indexBuffer;
    vk::DeviceMemory indexBufferMemory;

    void createVertexBuffer();
    void createIndexBuffer();
};


