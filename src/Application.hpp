#pragma once

#include "pch.hpp"
#include "Window.hpp"
#include "VkUtils.hpp"
#include "Renderer.hpp"

struct QueueFamilyIndices
{
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> presentFamily;

    bool isComplete()
    {
        return graphicsFamily.has_value() && presentFamily.has_value();
    }

    static QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice physDevice, vk::SurfaceKHR surface);
};

class Application
{
public:
    void run();

    Application(uint32_t width, uint32_t height, std::string title) : window{width, height, title}, title{title} {
        initVulkan();
    }
    virtual ~Application() { }

    Window window;

    vk::Instance instance;
    vk::SurfaceKHR surface;
    vk::PhysicalDevice physicalDevice;
    vk::Device device;
    vk::Queue graphicsQueue;
    vk::Queue presentQueue;
    vk::CommandPool commandPool;

    Renderer renderer = Renderer(*this);

protected:
    virtual void start() = 0;
    virtual void update(float deltaTime) = 0;

    virtual void cleanup();

private:
    void initVulkan();

    std::string title;

    vk::DebugUtilsMessengerEXT debugMessenger;
    float lastFrameTime;

    void createInstance();
    vk::PhysicalDevice getBestPhysicalDevice();
    void createLogicalDevice();
    void createCommandPool();

    std::vector<const char*> getRequiredExtensions();
    uint32_t ratePhysicalDevice(vk::PhysicalDevice physDevice);
    bool checkDeviceExtensionSupport(vk::PhysicalDevice device);

};


