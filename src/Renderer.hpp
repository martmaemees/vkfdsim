#pragma once

#include "Window.hpp"
#include "VkUtils.hpp"
#include "Mesh.hpp"
#include "Texture.hpp"

class Application;

struct Geometry {
    Mesh* mesh;
    std::optional<Texture*> texture;
};

class Renderer
{
public:
    Renderer(const Application& application) : application{application} { }

    void initRenderer();
    void cleanup();
    void drawGeometry(std::vector<Geometry> geom);

    Texture* createTexture(uint32_t width, uint32_t height);
    void bufferTextureData(Texture& tex, const float* data);

private:
    const Application& application;

    vk::SwapchainKHR swapchain;
    vk::Format swapchainImageFormat;
    vk::Extent2D swapchainExtent;
    std::vector<vk::Image> swapchainImages;
    std::vector<vk::ImageView> swapchainImageViews;

    vk::RenderPass renderPass;
    vk::PipelineLayout pipelineLayout;
    vk::Pipeline graphicsPipeline;
    std::vector<vk::Framebuffer> swapchainFramebuffers;
    std::vector<vk::CommandBuffer> commandBuffers;

    vk::DescriptorSetLayout descriptorSetLayout;

    std::vector<vk::Semaphore> imageAvailableSemaphores;
    std::vector<vk::Semaphore> renderFinishedSemaphores;
    std::vector<vk::Fence> inFlightFences;
    std::vector<vk::Fence> imagesInFlight;

    uint32_t currentFrame = 0;
    uint32_t maxFramesInFlight = 3;

    const vk::ClearColorValue clearColor = vk::ClearColorValue(std::array<float, 4>{
        0.0f, 0.0f, 0.0f, 0.0f
    });

    void createSwapchain();
    void createRenderPass();
    void createDescriptorSetLayout();
    void createGraphicsPipeline();
    void createFramebuffers();
    void createCommandBuffers();
    void createSyncObjects();
};


