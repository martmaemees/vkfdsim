#include "Texture.hpp"
#include "Application.hpp"

// TODO: Buffer image data.

void Texture::initTexture(
    uint32_t width, uint32_t height, uint32_t framesInFlight, vk::DescriptorSetLayout descriptorSetLayout
) {
    this->width = width;
    this->height = height;
    this->framesInFlight = framesInFlight;
    this->descriptorSetLayout = descriptorSetLayout;

    images.resize(framesInFlight);
    imageMemories.resize(framesInFlight);
    imageViews.resize(framesInFlight);
    stagingBuffers.resize(framesInFlight);
    stagingBufferMemories.resize(framesInFlight);
    transferCommandBuffers.resize(framesInFlight);

    bufferSize = width * height * 4;
    for (size_t i = 0; i < static_cast<size_t>(framesInFlight); i++) {
        // Create staging buffer.
        vk::BufferCreateInfo stagingBufferInfo(
            {}, bufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::SharingMode::eExclusive
        );
        stagingBuffers[i] = application.device.createBuffer(stagingBufferInfo);

        vk::MemoryRequirements stagingMemoryRequirements =
            application.device.getBufferMemoryRequirements(stagingBuffers[i]);
        vk::MemoryAllocateInfo stagingAllocInfo(
            stagingMemoryRequirements.size,
            findMemoryType(
                application.physicalDevice, stagingMemoryRequirements.memoryTypeBits,
                vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent
            )
        );
        stagingBufferMemories[i] = application.device.allocateMemory(stagingAllocInfo);
        application.device.bindBufferMemory(stagingBuffers[i], stagingBufferMemories[i], 0);

        // Create image.
        vk::ImageCreateInfo imageInfo(
            {}, vk::ImageType::e2D, vk::Format::eR32Sfloat, vk::Extent3D(width, height, 1), 1, 1,
            vk::SampleCountFlagBits::e1, vk::ImageTiling::eOptimal,
            vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst |
                vk::ImageUsageFlagBits::eTransferSrc,
            vk::SharingMode::eExclusive
        );
        images[i] = application.device.createImage(imageInfo);

        // Allocate image memory.
        vk::MemoryRequirements memReqs = application.device.getImageMemoryRequirements(images[i]);
        vk::MemoryAllocateInfo allocInfo(
            memReqs.size, findMemoryType(
                              application.physicalDevice, memReqs.memoryTypeBits,
                              //            vk::MemoryPropertyFlagBits::eHostVisible |
                              //            vk::MemoryPropertyFlagBits::eHostCoherent
                              vk::MemoryPropertyFlagBits::eDeviceLocal
                          )
        );
        imageMemories[i] = application.device.allocateMemory(allocInfo);
        application.device.bindImageMemory(images[i], imageMemories[i], 0);

        // Create image view.
        vk::ImageViewCreateInfo imgViewInfo(
            {}, images[i], vk::ImageViewType::e2D, vk::Format::eR32Sfloat, {},
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1)
        );
        imageViews[i] = application.device.createImageView(imgViewInfo);
    }

    // Create texture sampler.
    vk::SamplerCreateInfo samplerInfo(
        {}, vk::Filter::eLinear, vk::Filter::eNearest, vk::SamplerMipmapMode::eNearest,
        vk::SamplerAddressMode::eClampToBorder, vk::SamplerAddressMode::eClampToBorder,
        vk::SamplerAddressMode::eClampToBorder, 0.0f, VK_FALSE, 1.0f, VK_FALSE, vk::CompareOp::eAlways, 0.0f, 0.0f,
        vk::BorderColor::eFloatOpaqueBlack, VK_FALSE
    );
    sampler = application.device.createSampler(samplerInfo);

    // Create command buffer.
    transferCommandBuffers = application.device.allocateCommandBuffers(
        vk::CommandBufferAllocateInfo(application.commandPool, vk::CommandBufferLevel::ePrimary, framesInFlight)
    );

    initDescriptors();
}

void Texture::bufferTexture(const float* data, uint32_t frame, vk::Fence& waitFor) {
    size_t i = static_cast<size_t>(frame);

    void* stagingData = application.device.mapMemory(stagingBufferMemories[i], 0, bufferSize);
    memcpy(stagingData, data, static_cast<size_t>(bufferSize));
    application.device.unmapMemory(stagingBufferMemories[i]);

    application.device.waitForFences(1, &waitFor, VK_TRUE, UINT64_MAX);

    vk::CommandBuffer cb = transferCommandBuffers[i];
    cb.reset({});
    vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    cb.begin(beginInfo);

    // TODO: Transition image layout to transferDst
    vk::ImageMemoryBarrier toDstBarrier(
        {}, vk::AccessFlagBits::eTransferWrite, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal,
        VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, images[i],
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1)
    );
    cb.pipelineBarrier(
        vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, {}, {}, {}, toDstBarrier
    );

    // TODO: Copy buffer to image.
    vk::BufferImageCopy region(
        0, 0, 0, vk::ImageSubresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1), vk::Offset3D(0, 0, 0),
        vk::Extent3D(width, height, 1)
    );
    cb.copyBufferToImage(stagingBuffers[i], images[i], vk::ImageLayout::eTransferDstOptimal, region);

    // TODO: Transition image layout to shaderReadOnlyOptimal.
    vk::ImageMemoryBarrier toShaderRead(
        vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead, vk::ImageLayout::eTransferDstOptimal,
        vk::ImageLayout::eShaderReadOnlyOptimal, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, images[i],
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1)
    );
    cb.pipelineBarrier(
        vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, {}, {}, toShaderRead
    );

    cb.end();
    vk::SubmitInfo submitInfo(0, nullptr, {}, 1, &cb);
    application.graphicsQueue.submit(submitInfo, nullptr);
}

void Texture::cleanup() {
    application.device.destroyDescriptorPool(descriptorPool);
    application.device.destroyDescriptorSetLayout(descriptorSetLayout);

    application.device.destroySampler(sampler);
    application.device.freeCommandBuffers(application.commandPool, transferCommandBuffers);
    for (size_t i = 0; i < static_cast<size_t>(framesInFlight); i++) {
        application.device.destroyBuffer(stagingBuffers[i]);
        application.device.freeMemory(stagingBufferMemories[i]);

        application.device.destroyImageView(imageViews[i]);
        application.device.destroyImage(images[i]);
        application.device.freeMemory(imageMemories[i]);
    }
}

void Texture::initDescriptors() {
    // Create descriptor pool.
    vk::DescriptorPoolSize poolSize(vk::DescriptorType::eCombinedImageSampler, framesInFlight);
    vk::DescriptorPoolCreateInfo poolInfo({}, framesInFlight, 1, &poolSize);
    descriptorPool = application.device.createDescriptorPool(poolInfo);

    // Create descriptor sets.
    std::vector<vk::DescriptorSetLayout> layouts(framesInFlight, descriptorSetLayout);
    vk::DescriptorSetAllocateInfo allocInfo(descriptorPool, framesInFlight, layouts.data());
    descriptorSets = application.device.allocateDescriptorSets(allocInfo);

    for (size_t i = 0; i < descriptorSets.size(); i++) {
        vk::DescriptorImageInfo imageInfo(sampler, imageViews[i], vk::ImageLayout::eShaderReadOnlyOptimal);

        vk::WriteDescriptorSet descriptorWrite(
            descriptorSets[i], 0, 0, 1, vk::DescriptorType::eCombinedImageSampler, &imageInfo, nullptr, nullptr
        );
        application.device.updateDescriptorSets(descriptorWrite, nullptr);
    }
}
