#pragma once

/**
 * Temporarily using this file, since CLion doesn't currently support target_precompiled_headers.
 */

#include <string>
#include <iostream>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <chrono>
#include <optional>
#include <stdexcept>
#include <algorithm>

#include "vulkan.hpp"
#include "glm.hpp"

#include <GLFW/glfw3.h>
